class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post
  belongs_to :parent,  class_name: "Comment", optional: true
  has_many   :replies, class_name: "Comment", foreign_key: :parent_id, dependent: :destroy
  validates :body, presence: true, length: { in: 1..1000 }

  def self.search(search)
    if search.blank?
      Comment.none
    else
      Comment.where(['body LIKE ?', "%#{search}%"])
    end
  end
end
