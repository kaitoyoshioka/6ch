class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :post_category_relations, dependent: :destroy
  has_many :categories, through: :post_category_relations, dependent: :destroy
  validates :user_id, presence: true
  validates :content, presence: true
  validates :title, presence: true, length: { maximum: 60 }

  def self.search(search)
    if search.blank?
      Post.none
    else
      Post.where(['content LIKE ?', "%#{search}%"])
    end
  end
end
