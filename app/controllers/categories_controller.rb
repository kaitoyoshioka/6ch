class CategoriesController < ApplicationController
  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_back(fallback_location: root_path)
    else
      flash[:alert] = "カテゴリの作成に失敗しました"
      redirect_back(fallback_location: root_path)
    end
  end

  def show
    @categories = Category.all
    @category = Category.find(params[:id])
    @posts = @category.posts.includes([:user])
  end

  def category_params
    params.require(:category).permit(:name)
  end
end
