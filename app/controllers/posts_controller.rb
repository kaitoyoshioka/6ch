class PostsController < ApplicationController
  before_action :find_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def show
    @comments = @post.comments
    @comment = Comment.new
    @categories = Category.all
  end

  def new
    @post = Post.new
    @category = Category.new
  end

  def edit
    @category = Category.new
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to root_path, notice: "投稿に成功しました"
    else
      flash.now[:alert] = "投稿に失敗しました"
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to post_path, notice: "投稿を更新しました"
    else
      flash.now[:alert] = "投稿を更新できませんでした"
      render :edit
    end
  end

  def destroy
    if @post.destroy
      redirect_to root_path, notice: "投稿を削除しました"
    else
      redirect_to root_path, notice: "投稿を削除できませんでした"
    end
  end

  private

  def find_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:content, :title, :user_id, category_ids: [])
  end

  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to root_url if @post.nil?
  end
end
