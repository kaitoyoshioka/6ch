class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      redirect_to post_path(@comment.post), notice: 'コメントしました'
    else
      redirect_to post_path(@comment.post), notice: 'コメント失敗'
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.destroy
      flash[:alert] = "コメントを削除しました"
      redirect_back(fallback_location: root_path)
    else
      flash[:alert] = "コメントの削除に失敗しました"
      redirect_back(fallback_location: root_path)
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :post_id, :user_id, :parent_id)
  end
end
