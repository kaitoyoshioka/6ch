class SearchsController < ApplicationController
  def index
    @search = params[:search]
    @posts = Post.search(params[:search]).includes([:user])
    @comments = Comment.search(params[:search])
    @categories = Category.all
  end
end
