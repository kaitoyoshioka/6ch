class StaticpageController < ApplicationController
  def index
    @posts = Post.all.order(created_at: :desc).includes([:user])
    @categories = Category.all
  end
end
