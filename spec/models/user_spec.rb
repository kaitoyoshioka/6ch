require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }
  let(:post) { build(:post, user_id: user.id) }

  it "名前、メール、パスワードがあれば有効" do
    expect(user).to be_valid
  end

  describe "name" do
    it "名前がなければ無効" do
      user.name = " "
      expect(user).to be_invalid
    end

    it "50文字以上の名前は無効" do
      user.name = "a" * 50
      expect(user).to be_valid
      user.name = "a" * 51
      expect(user).to be_invalid
    end
  end

  describe "address" do
    it "メールアドレスがなければ無効" do
      user.email = " "
      expect(user).to be_invalid
    end

    it "正しくないメールアドレスは無効" do
      addresses = %w(user_at_foo.org example.user@foo.foo@bar_baz.com)
      addresses.each do |address|
        expect(build(:user, email: address)).to be_invalid
      end
    end

    it "重複したメールアドレスは無効" do
      create(:user, email: "aaron@example.com")
      user.email = "aaron@example.com"
      expect(user).to be_invalid
    end

    it "メールアドレスは大文字小文字を同じ値にしているか" do
      user.email = "Foo@ExAMPle.CoM"
      user.save!
      expect(user.reload.email).to eq "foo@example.com"
    end
  end

  describe "password" do
    it "パスワードがなければ無効" do
      user.password = user.password_confirmation = " "
      expect(user).to be_invalid
    end

    it "５桁以下のパスワードは無効" do
      user.password = user.password_confirmation = "a" * 6
      expect(user).to be_valid
      user.password = user.password_confirmation = "a" * 5
      expect(user).to be_invalid
    end
  end

  describe "関連しているモデル" do
    it "userを削除するとuserが書いたpostも削除される" do
      user.save
      post.save
      expect { user.destroy }.to change(Post, :count).by(-1)
    end

    it "userを削除するとuserが書いたcommentも削除される" do
      user.save
      post.save
      create(:comment, user_id: user.id, post_id: post.id)
      expect { user.destroy }.to change(Comment, :count).by(-1)
    end
  end
end
