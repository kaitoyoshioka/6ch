require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:user) { create(:user) }
  let(:post) { create(:post, user_id: user.id) }

  it "コンテント、タイトル、ユーザーがあれば有効" do
    expect(post).to be_valid
  end

  it "コンテントがなければ無効" do
    post.content = " "
    expect(post).to be_invalid
  end

  it "タイトルがなければ無効" do
    post.title = " "
    expect(post).to be_invalid
  end

  it "タイトルが60字以上は無効" do
    post.title = "a" * 61
    expect(post).to be_invalid
    post.title = "a" * 60
    expect(post).to be_valid
  end

  it "ユーザーがなければ無効" do
    post.user_id = " "
    expect(post).to be_invalid
  end
end
