require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:user) { create(:user) }
  let(:post) { create(:post, user_id: user.id) }
  let(:comment) { create(:comment, user_id: user.id, post_id: post.id) }

  it "内容、投稿、ユーザーがあれば有効" do
    expect(comment).to be_valid
  end

  it "内容がなければ無効" do
    comment.body = " "
    expect(comment).to be_invalid
  end

  it "ユーザーがなければ無効" do
    comment.user_id = " "
    expect(comment).to be_invalid
  end

  it "関連づけられた投稿がなければ無効" do
    comment.post_id = " "
    expect(comment).to be_invalid
  end
end
