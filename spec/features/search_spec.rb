require 'rails_helper'

RSpec.feature "Search", type: :feature do
  let!(:user) { create(:user) }
  let!(:post) { create(:post, user_id: user.id, content: 'こんにちわ') }
  let!(:post2) { create(:post, user_id: user.id, content: 'こんにちわんこ') }
  let!(:post3) { create(:post, user_id: user.id, content: 'おはよう') }
  let!(:comment) { create(:comment, body: 'こんこんにちわ', post_id: post.id, user_id: user.id) }
  let!(:comment2) { create(:comment, body: 'おやすみ', post_id: post2.id, user_id: user.id) }

  scenario "検索ワードに引っかかるPostとCommentのみが表示される" do
    visit root_path
    fill_in 'search', with: 'こんにちわ'
    find('#search_submit').click
    expect(page).to have_content post.content
    expect(page).to have_content post2.content
    expect(page).not_to have_content post3.content
    expect(page).to have_content comment.body
    expect(page).not_to have_content comment2.body
  end
end
