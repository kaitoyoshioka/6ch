require 'rails_helper'

RSpec.feature "Comments", type: :feature do
  let!(:user) { create(:user) }
  let!(:post) { create(:post, user_id: user.id) }
  let!(:post2) { create(:post, user_id: user.id) }
  let!(:comment) { create(:comment, body: 'test', post_id: post.id, user_id: user.id) }
  let!(:comment2) { create(:comment, body: 'テスト', post_id: post2.id, user_id: user.id) }

  scenario "Commentを投稿したらPostページに表示される" do
    login user
    visit post_path post
    expect(page).not_to have_content "こんにちわ"
    fill_in 'comment_area', with: 'こんにちわ'
    find('#comment_btn').click
    visit post_path post
    expect(page).to have_content "こんにちわ"
  end

  scenario "Postに紐付くCommentだけがPostページに表示される" do
    login user
    visit post_path post
    expect(page).to have_content comment.body
    expect(page).not_to have_content comment2.body
  end
end
