require 'rails_helper'

RSpec.feature "Posts", type: :feature do
  let!(:user) { create(:user) }

  scenario "Postを投稿したらトップページに表示される" do
    login user
    visit root_path
    expect(page).not_to have_content "コンテント"
    visit new_post_path
    fill_in 'post_title', with: 'タイトル'
    fill_in 'post_content', with: 'コンテント'
    click_on "投稿する"
    visit root_path
    expect(page).to have_content "コンテント"
  end
end
