require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:user) { create(:user) }
  let!(:category) { create(:category, name: "政治") }
  let!(:category2) { create(:category, name: "経済") }
  let!(:post) { create(:post, user_id: user.id, content: "post1") }
  let!(:post2) { create(:post, user_id: user.id, content: "post2") }
  let!(:post3) { create(:post, user_id: user.id, content: "post3") }
  let!(:relation) { create(:post_category_relation, post_id: post.id, category_id: category.id) }
  let!(:relation2) { create(:post_category_relation, post_id: post2.id, category_id: category.id) }
  let!(:relation3) { create(:post_category_relation, post_id: post3.id, category_id: category2.id) }
  let!(:relation4) { create(:post_category_relation, post_id: post.id, category_id: category2.id) }

  scenario "カテゴリページにはそのカテゴリのPostだけが表示される" do
    visit category_path category
    expect(page).to have_content post.content
    expect(page).to have_content post2.content
    expect(page).not_to have_content post3.content
  end

  scenario "複数のカテゴリを持つPostはそれぞれのカテゴリページで表示される" do
    visit category_path category
    expect(page).to have_content post.content
    visit category_path category2
    expect(page).to have_content post.content
  end
end
