require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe 'GET #show' do
    let(:category) { create(:category, name: "政治") }

    it 'リクエストが成功する' do
      get category_path category
      expect(response.status).to eq 200
    end
  end

  describe 'POST #create' do
    context 'パラメータが妥当な場合' do
      let(:valid_category) { post categories_path, params: { category: attributes_for(:category) } }

      it 'リクエストが成功する' do
        valid_category
        expect(response.status).to eq 302
      end
      it 'カテゴリが登録されること' do
        expect do
          valid_category
        end.to change(Category, :count).by(1)
      end
      it 'リダイレクトすること' do
        valid_category
        expect(response).to redirect_to root_path
      end
    end

    context 'パラメータが不正な場合' do
      let(:invalid_category) { post categories_path, params: { category: attributes_for(:category, :invalid) } }

      it 'リクエストが成功する' do
        invalid_category
        expect(response.status).to eq 302
      end
      it 'カテゴリが登録されない' do
        expect do
          invalid_category
        end.not_to change(Category, :count)
      end
    end
  end
end
