require 'rails_helper'

RSpec.describe "Posts", type: :request do
  let(:user) { create(:user) }

  describe 'GET #show' do
    let(:post) { create(:post, user: user) }

    it 'リクエストが成功すること' do
      get post_path post
      expect(response.status).to eq 200
    end

    it 'ユーザー名が表示されていること' do
      get post_path post
      expect(response.body).to include 'test-post'
    end
  end

  describe 'GET #new' do
    before do
      sign_in user
    end

    it 'リクエストが成功すること' do
      get new_post_path
      expect(response.status).to eq 200
    end
  end

  describe 'GET #edit' do
    let(:post) { create(:post, user: user) }

    before do
      sign_in user
      get edit_post_path post
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it 'タイトルが表示されていること' do
      expect(response.body).to include 'test-post-title'
    end

    it '本文が表示されていること' do
      expect(response.body).to include 'test-post'
    end
  end

  describe 'POST #create' do
    before do
      sign_in user
    end

    context 'パラメータが妥当な場合' do
      let(:valid_post) { post posts_path, params: { post: attributes_for(:post) } }

      it 'リクエストが成功する' do
        valid_post
        expect(response.status).to eq 302
      end

      it 'コメントが登録される' do
        expect do
          valid_post
        end.to change(Post, :count).by(1)
      end
    end

    context 'パラメータが不正な場合' do
      let(:invalid_post) { post posts_path, params: { post: attributes_for(:post, :invalid) } }

      it 'リクエストが成功する' do
        invalid_post
        expect(response.status).to eq 200
      end

      it 'コメントが登録されない' do
        expect { invalid_post }.not_to change(Post, :count)
      end

      it 'エラーが表示されること' do
        invalid_post
        expect(response.body).to include '投稿に失敗しました'
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:post) { create(:post, user: user) }

    before do
      sign_in user
    end

    it 'リクエストが成功すること' do
      delete post_path post
      expect(response.status).to eq 302
    end

    it '投稿が削除されること' do
      expect do
        delete post_path post
      end.to change(Post, :count).by(-1)
    end

    it 'トップページにリダイレクトすること' do
      delete post_path post
      expect(response).to redirect_to root_path
    end
  end
end
