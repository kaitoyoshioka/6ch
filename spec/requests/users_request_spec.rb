require 'rails_helper'

RSpec.describe "Users", type: :request do
  let(:user) { create(:user, email: "test@example.com") }

  describe 'GET #new' do
    it 'リクエストが成功すること' do
      get new_user_registration_path
      expect(response.status).to eq 200
    end
  end

  describe 'GET #edit' do
    before do
      sign_in user
      get edit_user_registration_path user
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it 'ユーザー名が表示されていること' do
      expect(response.body).to include 'testuser'
    end

    it 'メールアドレスが表示されていること' do
      expect(response.body).to include 'test@example.com'
    end
  end

  describe 'POST #create' do
    context 'パラメータが妥当な場合' do
      let(:valid_user) { post user_registration_path, params: { user: attributes_for(:user) } }

      it 'リクエストが成功すること' do
        valid_user
        expect(response.status).to eq 302
      end

      it 'ユーザーが登録されること' do
        expect { valid_user }.to change(User, :count).by(1)
      end

      it 'リダイレクトすること' do
        valid_user
        expect(response).to redirect_to root_path
      end
    end

    context 'パラメータが不正な場合' do
      let(:invalid_user) { post user_registration_path, params: { user: attributes_for(:user, :invalid) } }

      it 'リクエストが成功すること' do
        invalid_user
        expect(response.status).to eq 200
      end

      it 'ユーザーが登録されないこと' do
        expect { invalid_user }.not_to change(User, :count)
      end

      it 'エラーが表示されること' do
        invalid_user
        expect(response.body).to include '1 件のエラーが発生したため ユーザ は保存されませんでした'
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      sign_in user
    end

    it 'リクエストが成功すること' do
      delete user_registration_path
      expect(response.status).to eq 302
    end

    it 'ユーザーが削除されること' do
      expect do
        delete user_registration_path
      end.to change(User, :count).by(-1)
    end

    it 'トップページにリダイレクトすること' do
      delete user_registration_path
      expect(response).to redirect_to root_path
    end
  end
end
