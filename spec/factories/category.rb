FactoryBot.define do
  factory :category do
    name { "経済" }

    trait :invalid do
      name { "" }
    end
  end
end
