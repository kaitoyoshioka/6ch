FactoryBot.define do
  factory :post do
    content { "test-post" }
    title { "test-post-title" }
    user_id { 1 }

    trait :invalid do
      content { "" }
    end
  end
end
