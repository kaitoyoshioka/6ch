FactoryBot.define do
  factory :user do
    name { "testuser" }
    sequence(:email) { |n| "tester#{n}@example.com" }
    password { "foobar" }
    password_confirmation { "foobar" }

    trait :invalid do
      name { "" }
    end
  end
end
