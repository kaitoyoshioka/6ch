Rails.application.routes.draw do
  devise_for :users, :controllers => {
    :registrations => 'users/registrations',
    :sessions => 'users/sessions',
  }

  devise_scope :user do
    get "sign_in",  to: "users/sessions#new"
    get "sign_out", to: "users/sessions#destroy"
  end
  root 'staticpage#index'
  # get 'search/:id', to: "search#index"
  resources :posts, only: [:show, :new, :edit, :create, :update, :destroy] do
    resources :comments, only: [:create]
  end
  resources :comments, only: [:destroy]
  resources :categories, only: [:show, :create]
  resources :searchs, only: [:index]
end
